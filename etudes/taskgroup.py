import os
import sys
import logging
from curio import run, spawn, sleep
from curio.task import TaskGroup
from curio.errors import TaskCancelled, TaskGroupError
import curio.subprocess as csp
from cyanostracon.run_with_retry import StdinStream
from tempfile import TemporaryFile

async def long_wait(x):
    await sleep(10)
    return x

async def error(s, x):
    log = logging.getLogger('error task')
    for x in range(10*s):
        log.info('bla %f', x)
        await sleep(0.1)
    raise ValueError(x)
                        

async def run_multiple(argss):
    log = logging.getLogger('both')
    try:
        async with TaskGroup() as g:
            for args in argss:
                t = await g.spawn(csp.check_output(args, stderr=csp.STDOUT))
            async for t in g:
                try:
                    t.result
                    log.info('task %r completed', t)
                except csp.CalledProcessError as e:
                    async with StdinStream() as stdin:
                        log.error('task %r, running %r, exited with nonzero '
                                  'code %d', t, e.cmd, e.returncode)
                        print(e.stdout.decode(sys.stdout.encoding),
                              file=sys.stderr)
                        valid_input = False
                        while not valid_input:
                            print('Something went wrong when running '
                                  'command {!r} (exitcode {}). Try again? '
                                  '[Y/n] '.format(e.cmd, e.returncode),
                                  end='')
                            sys.stdout.flush()
                            the_input = await stdin.readline()
                            the_input = the_input.strip().decode(
                                sys.stdin.encoding)
                            if the_input == '':
                                valid_input = True
                                try_again = True
                            elif (the_input.startswith('y')):
                                valid_input = True
                                try_again = True
                            elif (the_input.startswith('n')):
                                valid_input = True
                                try_again = False
                                log.error('giving up; re-raising error')
                                raise
                            if not valid_input:
                                print('Did not understand your input. '
                                      'Asking again.')
                        if try_again:
                            log.warning('spawning another! %r', e.cmd)
                            await g.spawn(csp.check_output(e.cmd,
                                                           stderr=csp.STDOUT))
    except TaskGroupError as e:
        for t in e:
            print('error: task', t, 'exception is', t.exception)
            print('           ', t, 'result is', t.result)
    
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    run(both, (('python', 'succeeder.py', '42'),
               ('python', 'failer.py', '2'),
               ('python', 'failer.py', '3'),
               ('python', 'succeeder.py', '10')),
        debug=())
    
