import os
import logging
from random import getrandbits
from curio.channel import Channel
from curio import spawn, run
import sys

class UnexpectedResponse(Exception):
    pass

async def page(authkey, *args):
    log = logging.getLogger('page')
    l = Channel(('127.0.0.1', 1337))
    ch = await l.connect(authkey=authkey)
    for arg in args:
        exclamation = ('hey', arg)
        log.info('exclaiming %r', exclamation)
        await ch.send(exclamation)
    log.info('ending')
    await ch.send(('end',))
    log.info('ended')
    await l.close()
    
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    authkey = os.getenv('CYANOSTRACON_IPC_AUTHKEY').encode('UTF-8')
    logging.info('authkey is %r', authkey)
    run(page(authkey, *sys.argv[1:]))
