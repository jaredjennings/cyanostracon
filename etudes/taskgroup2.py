"""Study interaction of ready, finished and giving up.

We have a queue of things to do (call them intents), and a set of
machines to do them with. Each machine can signal that it is ready for
an intent. Each pairing of an intent with a machine is a mint. A mint
may fail; if so, we may want to stop our entire effort. If we stop,
all live mints should be cancelled. If not, the failed intent goes
back on the queue.

When the queue is empty, or we have stopped abnormally, we are done.

Readiness of a machine has some correspondence with finishing of a
mint (successful or not).

"""

import os
import sys
import logging
from curio import run, spawn
from curio.queue import Queue, PriorityQueue
from curio.task import TaskGroup, TaskGroupError, TaskError
from curio.subprocess import CompletedProcess
from collections import defaultdict

from curio import sleep
import random
from curio.sync import Lock

class Fail(Exception):
    pass

async def just_try_again(spec, result):
    log = logging.getLogger('just_try_again')
    log.debug('%s', spec)
    return True

async def just_say_no(spec, result):
    log = logging.getLogger('just_say_no')
    log.debug('%s', spec)
    return False

class MultiburnCommon:

    FAILED = 0
    NORMAL = 1

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)

    async def _resource_ready(self, readyq, statusq):
        """Put things on readyq and statusq when a resource is ready."""
        raise NotImplemented()

    async def underlying_task(self, resource, spec):
        """Do a thing, and return a CompletedProcess."""
        raise NotImplemented()

    async def provide_for_task(self, readyq, backlogq, statusq, doneq, spec):
        """Wait until a resource is ready; try; re-backlog spec if fail."""
        log = self.log.getChild('provide_for_task')
        _blank, resource = await readyq.get()
        await statusq.put((resource, 'busy'))
        result = await self.underlying_task(resource, spec)
        log.debug('coordinate finished: %r', result)
        log.debug('return code: %r', result.returncode)
        if result.returncode == 0:
            log.info('%s finished OK', spec)
            await statusq.put((resource, 'finished'))
            await doneq.put(spec)
        else:
            log.info('%s failed', spec)
            await statusq.put((resource, 'FAILED'))
            await backlogq.put((self.FAILED, spec, result))

    async def _starter(self, readyq, backlogq, statusq, doneq, problems_quitp):
        """Start providing for tasks on the backlog."""
        log = self.log.getChild('starter')
        while True:
            dontcare, spec, result = await backlogq.get()
            if result is None:
                # first time through
                log.debug('%s for the first time', spec)
                await spawn(self.provide_for_task(readyq, backlogq, statusq, doneq, spec),
                            daemon=True)
            else:
                # a fail
                if await problems_quitp(spec, result):
                    log.debug('%s again', spec)
                    await spawn(self.provide_for_task(readyq, backlogq, statusq, doneq, spec),
                                daemon=True)
                else:
                    log.warning('not retrying!')
                    raise Fail()

    async def _finisher(self, n_to_finish, doneq):
        """Figure out when we are done."""
        log = self.log.getChild('finisher')
        done = []
        while True:
            d = await doneq.get()
            done.append(d)
            log.debug('finished: %r', done)
            if len(done) == n_to_finish:
                log.info('all done!')
                break

    async def _statuser(self, statusq):
        """Show running status of all the resources."""
        log = self.log.getChild('status')
        status_chars = defaultdict(lambda: '?')
        status_chars['ready'] = '_'
        status_chars['busy'] = ';'
        status_chars['finished'] = '.'
        status_chars['FAILED'] = '&'
        statuses = {}
        while True:
            resource, status = await statusq.get()
            statuses[resource] = status
            log.info('   ' + '   '.join('{} {}'.format(m, status_chars[s])
                                        for m, s in sorted(statuses.items())))

    async def multiburn(self, specs, problems_quitp):
        log = self.log.getChild('g')
        readyq = Queue()
        backlogq = PriorityQueue()
        doneq = Queue()
        statusq = Queue()
        for s in specs:
            await backlogq.put((self.NORMAL, s, None))
        parts = TaskGroup()
        await parts.spawn(self._finisher(len(specs), doneq))
        await parts.spawn(self._statuser(statusq))
        await parts.spawn(self._starter(readyq, backlogq, statusq, doneq,
                                        problems_quitp))
        await parts.spawn(self._resource_ready(readyq, statusq))
        await parts.join(wait=any)
        log.debug('got control back')
        await parts.cancel_remaining()
        return



class MultiburnEtude(MultiburnCommon):
    words = 'AVAUATUH AWAUATUI D$PL L$0D grab window fseek D$x9 D$(A dixGetPrivate McB0S$BN dHoI8D 58Xu FpDH'.split()
    resources = ('1', '2', '3', '4', '5')
    timescale = 0.1

    def __init__(self):
        super().__init__()
        # the inuse and ready dictionaries let our fake
        # _resource_ready know what resources it shouldn't say are
        # ready. in case of a real optical drive, the drive would
        # never say a blank disc had been inserted into it while it is
        # burning a disc. these things help _resource_ready imitate
        # that behavior.
        self.inuse = defaultdict(lambda: False)
        self.ready = defaultdict(lambda: False)
        self.inuse_l = Lock()
        self.ready_l = Lock()

    async def underlying_task(self, resource, spec):
        log = self.log.getChild('underlying_task')
        async with self.inuse_l:
            self.inuse[resource] = True
        async with self.ready_l:
            self.ready[resource] = False
        duration = random.choice([self.timescale*11, self.timescale*13,
                                  self.timescale*11])
        will_fail = random.choice([True, False, False])
        log.debug('beginning %s on %s. failing? %s. duration %s.',
                 spec, resource, will_fail, duration)
        await sleep(duration)
        exitcode = 4 if will_fail else 0
        result = CompletedProcess('command {} {}'.format(resource, spec),
                                  exitcode,
                                  'output {} {}'.format(resource, spec),
                                  'stderr {} {}'.format(resource, spec))
        async with self.inuse_l:
            self.inuse[resource] = False
        return result
    
    async def _resource_ready(self, readyq, statusq):
        log = self.log.getChild('resource_ready')
        while True:
            await sleep(self.timescale*3)
            async with self.inuse_l:
                async with self.ready_l:
                    available = [r for r in self.resources
                                 if not self.inuse[r] and not self.ready[r]]
                    if available:
                        become_ready = random.choice(available)
                        self.ready[become_ready] = True
                        log.debug('%r ready', become_ready)
                        await readyq.put(('ready', become_ready))
                        await statusq.put((become_ready, 'ready'))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    e = MultiburnEtude()
    run(e.multiburn(e.words, just_say_no))
