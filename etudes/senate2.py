import os
import sys
import logging
from random import getrandbits
from curio.channel import Channel, AuthenticationError
from curio import spawn, run, sleep
from curio.subprocess import Popen
from curio.queue import Queue

# sentinel
End = lambda: None

async def senate(authkey):
    log = logging.getLogger('senate')
    l = Channel(('127.0.0.1', 1337))
    while True:
        try:
            ch = await l.accept(authkey=authkey)
        except AuthenticationError as e:
            log.error('rejecting connection: %s', e)
        else:
            q = Queue()
            await spawn(clerk(ch, q))
            i = await q.get()
            while i is not End:
                log.info('message is %r', i)
                if i == ('end',):
                    break
                i = await q.get()

async def clerk(ch,q):
    log = logging.getLogger('clerk')
    log.info('channel opened %r', ch)
    exclamation = await ch.recv()
    while exclamation != ('end',):
        log.info('the page said %r', exclamation)
        await q.put(exclamation)
        exclamation = await ch.recv()
    await q.put(End)
        
async def govt():
    log = logging.getLogger('govt')
    authkey = '{:x}'.format(getrandbits(256))
    authkey_bytes = authkey.encode('UTF-8')
    log.info('authkey is %s', authkey)
    my_senate = await spawn(senate(authkey_bytes))
    log.info('senate spawned. sleeping')
    await sleep(2)
    log.info('spawning page')
    os.environ['CYANOSTRACON_IPC_AUTHKEY'] = authkey
    async with Popen(
            ['python', 'page2.py', 'foo', 'bar', 'baz']) as p:
        pass
    await sleep(3)
    async with Popen(
            ['python', 'page2.py', 'foo', 'bar', 'baz']) as p:
        pass
    await my_senate.cancel()
    
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    run(govt())
