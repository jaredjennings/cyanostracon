from multiprocessing import Pipe, Queue
from curio.workers import run_in_process
from curio.channel import Connection
from curio import spawn, sleep, run
import time

def pipe_echo_process(pipe):
    while True:
        print('- i bet i\'m going to block')
        message = pipe.recv()
        print('- got', message, ' - waiting a bit')
        time.sleep(1.0)
        pipe.send(message)
        print('- sent', message)

async def regular_talker():
    tick = 0.5
    t = time.time()
    while True:
        t2 = time.time()
        dt = t2 - t
        if abs(dt - tick) > 0.05:
            print('        - long tick, {dt:4.2f} seconds '
                  '(should be {tick:4.2f})'.format(
                      dt=dt, tick=tick))
        t = t2
        await sleep(tick)

async def main():
    rtt = await spawn(regular_talker)
    parent, child = Pipe()
    parent = Connection.from_Connection(parent)
    echo_task = await spawn(run_in_process(pipe_echo_process, child))
    print('sleeping after spawn')
    await sleep(2)
    print('sending one')
    await parent.send('one')
    print('receiving')
    x = await parent.recv()
    print('got {}'.format(x))
    print('sleeping')
    await sleep(3)
    print('sending two')
    await parent.send('two')
    print('receiving')
    x = await parent.recv()
    print('got {}'.format(x))
    print('sleeping')
    await sleep(1)
    print('cancelling echo task')
    await echo_task.cancel()
    print('sleeping 5')
    await sleep(5)

    print('cancelling talker')
    await rtt.cancel()

if __name__ == '__main__':
    run(main())
