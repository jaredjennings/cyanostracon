import os
import sys
import logging
from random import getrandbits
from curio.channel import Listener, AuthenticationError
from curio import spawn, run, sleep
from curio.subprocess import Popen

async def senate(authkey):
    log = logging.getLogger('senate')
    l = Listener(('127.0.0.1', 1337), authkey=authkey)
    while True:
        try:
            ch = await l.accept()
        except AuthenticationError as e:
            log.error('rejecting connection: %s', e)
        else:
            await spawn(clerk(ch))

async def clerk(ch):
    log = logging.getLogger('clerk')
    print('channel opened', ch)
    exclamation = await ch.recv()
    log.info('the page said %r', exclamation)
    log.info('sleeping eh')
    await sleep(5)
    await ch.send(('continue',))

async def govt():
    log = logging.getLogger('govt')
    authkey = '{:x}'.format(getrandbits(256))
    authkey_bytes = authkey.encode('UTF-8')
    log.info('authkey is %s', authkey)
    my_senate = await spawn(senate(authkey_bytes))
    log.info('senate spawned. sleeping')
    await sleep(5)
    log.info('spawning page')
    os.environ['CYANOSTRACON_IPC_AUTHKEY'] = authkey
    async with Popen(
            ['python', 'page.py', 'foo', 'bar', 'baz']) as p:
        pass
    await sleep(10)
    await my_senate.cancel()
    
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    run(govt())
