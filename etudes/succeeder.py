import logging
import sys
import os

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    log = logging.getLogger('succeeder[{}]'.format(os.getpid()))
    how_long = int(sys.argv[1])
    for x in range(how_long*10):
        log.info('tenth of a second %d', x)
    log.info('succeeding')
    sys.exit(0)
