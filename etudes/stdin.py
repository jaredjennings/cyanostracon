import os
import sys
import logging
from curio.io import FileStream
from curio import run, spawn, sleep
from curio.errors import TaskCancelled

class StdinStream(FileStream):
    def __init__(self):
        super().__init__(sys.stdin.buffer)

    async def __aexit__(self, *args):
        await self.flush()
        # don't close stdin
        os.set_blocking(int(self._fileno), True)
        self._file = None
        self._fileno = -1

async def get_some_input():
    log = logging.getLogger('get_some_input')
    async with StdinStream() as s:
        count = 1
        while True:
            l = await s.readline()
            log.info('%d: %s', count, l.strip())
            count += 1
            if l.strip() == b'':
                break
        log.info('quitting')

async def annoying_output():
    log = logging.getLogger('annoying_output')
    count = 1
    while True:
        try:
            await sleep(1.0)
            log.info('%d', count)
            count += 1
        except TaskCancelled:
            break

async def both():
    ao = await spawn(annoying_output())
    await sleep(0.5)
    gsi = await spawn(get_some_input())
    await gsi.join()
    gsi = await spawn(get_some_input())
    await gsi.join()
    await ao.cancel()
    await ao.join()
    
    
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    run(both)
    x = input('Hey this is a prompt')
    print(x)
    
