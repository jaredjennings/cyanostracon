import os
import logging
from random import getrandbits
from curio.channel import Listener, Client
from curio import spawn, run
import sys

class UnexpectedResponse(Exception):
    pass

async def page(authkey, *args):
    log = logging.getLogger('page')
    l = await Client(('127.0.0.1', 1337), authkey=authkey)
    exclamation = ('dar_params', *args)
    log.info('exclaiming %r', exclamation)
    await l.send(exclamation)
    log.info('waiting for continue')
    reply = await l.recv()
    log.info('received %r', reply)
    if reply != ('continue',):
        raise UnexpectedResponse(reply, 'to', exclamation)
    log.info('ending')

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    authkey = os.getenv('CYANOSTRACON_IPC_AUTHKEY').encode('UTF-8')
    logging.info('authkey is %r', authkey)
    run(page(authkey, *sys.argv[1:]))
