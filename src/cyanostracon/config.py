# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Make settings available to this program and to the future.

Guiding principles:

0. Opinionate where possible (e.g. digits).
1. Provide defaults where opinionation is unwise (e.g. redundancy).
2. Let the user set common configuration values with command-line
   switches.
3. Let the user override defaults with a config file, because it's
   natural to write a darrc and because it's how the written-down
   configuration (see next item) gets used next time.
4. Write down every configuration value - defaults,
   user-written-config file, config from command-line switches -
   on the backup media, so there is a record of how the backup
   was made.

The set of optical drives that can be used is not part of the
configuration: it will differ from machine to machine, and be detected
on each run.

The configuration is made available to the rest of cyanostracon by the
use of some objects that make accesses nicer: by making items
available as attributes (e.g. config.x == config['x']), by
auto-converting units (e.g. config.x_KiB == config.x_MiB * 1024), and
by calculating some values from other given values (slice size, at
this writing).

"""

from collections import Mapping
import logging
from pkg_resources import resource_stream
from os import getenv
from os.path import join, exists, abspath
# import yaml
from io import TextIOWrapper

# Some of these can be overridden with command-line options. All of
# them can be overridden with a config file. The complete
# configuration will be written in cleartext on every backup disc.
defaults = {
    'darrc': '''\
# Encrypt symmetrically. DO NOT specify the key here, nor in any other
# configuration file: both defaults, and all configuration, are burned
# on every disc in cleartext.
--key aes:
# don't back up caches, e.g. Firefox cache
--cache-directory-tagging
-v
create:
--compression=bzip2
extract:
-O
''',
    'digits': 6,
    'slices_per_disc': 500,
    'disc_size_MiB': 23841,
    # valid values: see http://storaged.org/doc/udisks2-api/latest/gdbus-org.freedesktop.UDisks2.Drive.html#gdbus-property-org-freedesktop-UDisks2-Drive.MediaCompatibility
    'disc_media': 'optical_bd_r',
    'reserve_space_KiB': 10240,
    'data_discs': 5,
    'parity_discs': 2,
    'actually_burn': True,
}

calculated = {
    'slices_per_set': lambda x: x['data_discs'] * x['slices_per_disc'],
    'discs_per_set': lambda x: x['data_discs'] + x['parity_discs'],
    'scratch_free_needed_MiB': lambda x: (
        (x['data_discs'] + x['parity_discs']) * x['disc_size_MiB']),
    'number_format': lambda x: '{:0' + str(x['digits']) + '}',
    'slice_size_KiB': lambda x: ((x['disc_size_MiB'] * 1024 -
                                  x['reserve_space_KiB']) /
                                 x['slices_per_disc']),
}


class SomeCalculated(Mapping):
    """Automatically calculate some items.

    Objects of this class are read-only, so make the dictionary you
    want and then hand it here.

    """
    def __init__(self, source_dict, calculated):
        self._here = source_dict
        self._calculated = calculated

    def __getitem__(self, name):
        if name in self._here:
            return self._here[name]
        elif name in self._calculated:
            return self._calculated[name](self._here)
        else:
            raise KeyError(name)

    def __len__(self):
        return len(self._here) + len(self._calculated)

    def __iter__(self):
        for k in self._here:
            yield k
        for k in self._calculated:
            yield k

    def __repr__(self):
        return 'SomeCalculated({!r},{!r})'.format(self._here, self._calculated)

class UnitConverter(Mapping):
    """Automatically convert between kibibytes, mebibytes and gibibytes.

    Objects of this class are read-only, so make the dictionary you
    want and then hand it here.

    """
    
    conversions_to = {
        '_KiB': {'_MiB': 1024, '_GiB': 1048576},
        '_MiB': {'_KiB': 1/1024, '_GiB': 1024},
        '_GiB': {'_KiB': 1/1048576, '_MiB': 1/1024},
        }
    conversions_from = {
        '_GiB': {'_KiB': 1048576, '_MiB':1024},
        '_MiB': {'_KiB': 1024, '_GiB': 1/1024},
        '_KiB': {'_MiB': 1/1024, '_GiB': 1/1048576},
        }
    
    def __init__(self, source_dict):
        self._here = source_dict
        
    def __getitem__(self, name):
        if name in self._here:
            return self._here[name]
        else:
            for to_suffix, froms in self.conversions_to.items():
                if name.endswith(to_suffix):
                    stem = name[:-len(to_suffix)]
                    for from_suffix, factor in froms.items():
                        maybe = stem + from_suffix
                        if maybe in self._here:
                            return self._here[maybe] * factor
                    else:
                        raise KeyError(name)
            else:
                raise KeyError(name)

    def __iter__(self): # this is just the keys
        for k in self._here:
            yield k
            for from_suffix, tos in self.conversions_from.items():
                for to_suffix, factor in tos.items():
                    if k.endswith(from_suffix):
                        definitely = k[:-len(from_suffix)] + to_suffix
                        yield definitely

    def __contains__(self, name):
        if name in self._here:
            return True
        else:
            for to_suffix, froms in self.conversions_to.items():
                if name.endswith(to_suffix):
                    stem = name[:-len(to_suffix)]
                    for from_suffix, factor in froms.items():
                        if stem + from_suffix in self._here:
                            return True
            return False            
    
    def __len__(self):
        l = 0
        for k in self: # see __iter__ above
            l += 1
        return l

    def __repr__(self):
        return 'UnitConverter({!r})'.format(self._here)


class ItemsToAttributes:
    """Make items available as attributes. Read-only."""
    
    def __init__(self, source_dict):
        self._here = source_dict

    def __getattr__(self, name):
        if name in self._here:
            return self._here[name]
        else:
            raise AttributeError(name)

    def __repr__(self):
        return 'ItemsToAttributes({!r}'.format(self._here)


def usable_config(raw_config):
    with_defaults = {}
    with_defaults.update(defaults)
    with_defaults.update(raw_config)
    return ItemsToAttributes(
        UnitConverter(
            SomeCalculated(with_defaults, calculated)))

