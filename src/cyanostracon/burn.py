import sys
from os.path import basename
import logging
from multiprocessing import Pipe, Process
from collections import defaultdict
from insertdisc.udisks2.queue import wait_for_disc_process
from curio import run, spawn
from curio.queue import Queue, PriorityQueue
from curio.task import (
    TaskGroup, TaskGroupError, TaskError, CancelledError, timeout_after,
    TaskTimeout)
from curio.workers import run_in_process
from curio.channel import Connection
import curio.subprocess as csp
from cyanostracon.run_with_retry import yesno


# These constants help us retry failed burns before doing additional
# new burns.
RETRYING = 0
FIRST_TIME = 1

status_chars = defaultdict(lambda: '?')
status_chars.update({
    'ready':    ':]',
    'busy':     ':d',
    'finished': ':D',
    'FAILED':   ':(',
})


async def curio_blank_disc_adapter(readyq, statusq,
                                   ofud2drive_media, *waiter_args):
    """Detect blank discs.

    The insertdisc library is used to detect blank discs of the given
    ofud2drive_media type already inserted at the time multiburn
    begins, and thereafter. As soon as a blank disc of the proper type
    is available, a burn is started. Burns are done using growisofs
    with run_with_retry, so if a burn fails ...! you will put in a new
    blank disc and another burn will start on it instead.

    """
    log = logging.getLogger('curio_blank_disc_adapter')
    log.debug('started')
    mine, childs = Pipe()
    detector_task = await spawn(run_in_process(
        wait_for_disc_process, childs, *waiter_args), daemon=True)
    log.debug('spawned wait_for_disc_process which is %r', wait_for_disc_process)
    mine = Connection.from_Connection(mine)
    log.debug('sending query')
    await mine.send(('blank', ofud2drive_media))
    await mine.send(('go',))
    log.debug('awaiting blanks')
    while True:
        try:
            _blank, devpath = await mine.recv()
            await readyq.put(devpath)
            await statusq.put((devpath, 'ready'))
        except CancelledError:
            await detector_task.cancel()
            break


class BurnFailed(Exception):
    pass

async def just_try_again(spec, result):
    log = logging.getLogger('just_try_again')
    log.debug('%s', spec)
    return True

async def just_say_no(spec, result):
    log = logging.getLogger('just_say_no')
    log.debug('%s', spec)
    return False

async def ask_user(spec, result):
    print(result.stdout, file=sys.stderr)
    print(result.stderr, file=sys.stderr)
    return await yesno('Retry? [Y/n]', True)


async def burn_task(resource, spec):
    title, dir = spec
    return await csp.run(('growisofs', '-Z', resource,
                          '-R', '-J', '-V', title, dir))


async def _provide_for_task(readyq, backlogq, statusq, doneq, spec):
    """Wait until a drive has a blank; burn; re-backlog spec if fail.

    This task blocks while the burn is happening.
    """
    log = logging.getLogger('burn._provide_for_task')
    resource = await readyq.get()
    await statusq.put((resource, 'busy'))
    result = await burn_task(resource, spec)
    if result.returncode == 0:
        log.info('%r finished OK', spec)
        await statusq.put((resource, 'finished'))
        await doneq.put((spec, resource))
    else:
        log.info('%r failed', spec)
        await statusq.put((resource, 'FAILED'))
        await backlogq.put((RETRYING, spec, result))


async def _starter(readyq, backlogq, statusq, doneq, problems_retryp):
    """Start providing for tasks on the backlog."""
    log = logging.getLogger('burn._starter')
    while True:
        dontcare, spec, result = await backlogq.get()
        if result is None:
            # first time through
            log.debug('%s for the first time', spec)
            await spawn(_provide_for_task(readyq, backlogq, statusq, doneq, spec),
                        daemon=True)
        else:
            # a fail
            if await problems_retryp(spec, result):
                log.debug('%s again', spec)
                await spawn(provide_for_task(readyq, backlogq, statusq, doneq, spec),
                            daemon=True)
            else:
                log.warning('not retrying!')
                raise BurnFailed(spec, result)

async def _finisher(n_to_finish, doneq):
    """Figure out when we are done."""
    log = logging.getLogger('burn._finisher')
    log.debug('waiting for %d burns to finish', n_to_finish)
    done = []
    while True:
        d = await doneq.get()
        done.append(d)
        spec, resource = d
        title, dir = spec
        log.info('burn of disc %s finished in %s; ejecting', title, resource)
        await eject(resource)
        log.debug('burns finished: %d of %d', len(done), n_to_finish)
        if len(done) == n_to_finish:
            log.info('all done!')
            break

async def eject(resource):
    await csp.run(['eject', resource])

async def _status(statusq):
    """Show running status of all the resources."""
    log = logging.getLogger('burn._status')
    statuses = {}
    while True:
        async with timeout_after(15):
            try:
                resource, status = await statusq.get() # blocks till change is put
                resource = basename(resource) # strip off /dev/ if it is there
                statuses[resource] = status
            except TaskTimeout:
                pass
        log.info('   ' + '   '.join('{} {}'.format(m, status_chars[s])
                                    for m, s in sorted(statuses.items())))

async def multiburn(specs, ofud2drive_media,
                    problems_retryp, blank_waiter, *waiter_args):
    """Burn multiple discs at once.

    specs is a sequence of tuples (t, d), where t is a disc title
    (<=16 chars because of Joliet), and d is the full pathname of a
    directory containing the files that should go on the
    disc. ofud2drive_media is the type of media onto which to burn,
    e.g. 'optical_bd_r'; see
    <http://storaged.org/doc/udisks2-api/latest/gdbus-org.freedesktop.UDisks2.Drive.html#gdbus-property-org-freedesktop-UDisks2-Drive.MediaCompatibility>.

    blank_waiter is a long-running coroutine that takes three
    arguments readyq, statusq, and ofud2drive_media, plus waiter_args,
    used during testing. Whenever a blank disc is ready for drive
    /dev/foo, waiter should put '/dev/foo' onto readyq, and ('ready',
    '/dev/foo') onto statusq. ofud2drive_media is as above.

    If a burn fails, there is a question whether we should continue:
    if the problem is correctable, we should not give up; if not, we
    should fail fast, to get out of the way and avoid wasting
    media. But there is no way for this coroutine to know which has
    happened. So it awaits an answer from problems_retryp, passing it
    the spec (attempted disc title, burn directory), and the result of
    the failed growisofs (a CompletedProcess). If the answer is True,
    we retry; if False, a BurnFailed exception is raised, with the
    spec and result.

    Failed burns are placed first on the queue, so that if a burn
    error lies in the inputs to growisofs and not in a media fault, we
    can fail faster.

    It is assumed that drive problems are rare enough that if they
    happen, we can fail and start over rather than dealing with them
    using additional code complexity (e.g. continuing without using
    the failed drive, ascertaining whether the drive failed, etc).

    """
    log = logging.getLogger('burn.multiburn')
    readyq = Queue()
    backlogq = PriorityQueue()
    doneq = Queue()
    statusq = Queue()
    for s in specs:
        await backlogq.put((FIRST_TIME, s, None))
    parts = TaskGroup()
    await parts.spawn(_finisher(len(specs), doneq))
    await parts.spawn(_status(statusq))
    await parts.spawn(_starter(readyq, backlogq, statusq, doneq,
                                    problems_retryp))
    await parts.spawn(blank_waiter(readyq, statusq,
                                   ofud2drive_media, *waiter_args))
    log.debug('all subtasks spawned')
    await parts.join(wait=any)
    log.debug('got control back')
    await parts.cancel_remaining()
    return
