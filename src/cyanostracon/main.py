# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import time
import logging
import getopt
import yaml
from curio import run
# Use of the subcommand decorator on functions in modules we import
# will populate ``subcommands``.
from cyanostracon.subcommand import subcommands, UnknownSubcommand
import cyanostracon.dar
from cyanostracon.config import defaults

def usage(message=''):
    print(""" {message}
    
    Usage: python3 {progname} [OPTION...] -- dar DAR_PARAMETERS
    
    Options:
    -h, --help                    This help.
    -t, --test                    Run unit tests instead of normal operation.
    -n, --dry-run                 Don't actually burn discs; make directories
    containing files that would have been burned.
    (May use much more scratch space.)
    -v, --verbose                 More messages; show dar command run, darrc.
    -c, --config=myconfig.yaml    Read a YAML config file. See other
    documentation regarding its contents.
    --disc-size-MiB=x         Burn discs of size x. Default {def_disc_size}.
    "25GB"=23841; "700MB"=680; "4.7GB"=4482.
    --data-discs=m            Make each disc set contain m data discs.
    --parity-discs=n          Make each disc set contain n parity discs.
    Wise values depend on media quality and
    expected backup shelf life. Default m={m}, n={n}.
    --scratch-dir=myscratch   Set scratch directory. This directory must have
    (m+n)*x MiB free, plus a little.
    
    Dar parameters of note:
    Creating archive:   -c <archive basename> -R <dir with files to backup>
    Extracting archive: -x <archive basename>
    
    You get 23 characters for the archive basename. (Sorry, that's ISO
    9660.) See dar(1) about parameters you can give to dar. Don't get
    fancy: only use the ones that tell dar which mode to operate in, and
    which files to archive.  Otherwise this script will not form a
    complete record of how dar was run.
    
    This program makes compressed, encrypted backups, consisting of one or
    more disc sets, each with some data discs and some parity discs. It
    requires the following software (or later versions): Python 3.5; dar
    2.5.4*; parchive 1.1; growisofs 7.1; genisoimage 1.1.11.
    
    * If you are not using encryption, any recent dar will do (2.4.8 did
    fine without encryption, for example). If you are encrypting, you need
    change 8e64f413. dar 2.5.4 and later contain this change. See
    <https://sourceforge.net/p/dar/bugs/183/> for more.
    """.format(message=message, progname=sys.argv[0]),
          file=sys.stderr)
    sys.exit(1)


def _logging_setup(loglevel):
    # style only influences how format is interpreted, not also how values are
    # interpolated into log messages. source: Python 3.2
    # logging/__init__.py:317, LogRecord class, getMessage method.
    fmt = logging.Formatter(style='{', fmt='cyanostracon {process} '
                        '[{asctime}] {name}: {message}')
    stream = logging.StreamHandler(sys.stderr)
    stream.setFormatter(fmt)
    root_logger = logging.getLogger()
    root_logger.addHandler(stream)
    root_logger.setLevel(loglevel)
    # dar expects its stdin and stdout to be a terminal, and it's
    # going to be running this program, so we will make a log file for
    # our messages rather than depend on redirection
    logfile = logging.FileHandler('cyanostracon.log.{}'.format(os.getpid()))
    logfile.setFormatter(fmt)
    root_logger.addHandler(logfile)


if __name__ == '__main__':
    try:
        dashdash = argv.index('--')
        switches = argv[1:dashdash]
        subcommand_etc = argv[dashdash+1:]
    except ValueError:
        switches = argv[1:]
        subcommand_etc = []
    opts, remaining = getopt.getopt(switches, 'hvntc:',
                                    ['help', 'verbose', 'dry-run', 'test',
                                     'config', 'disc-size-MiB',
                                     'data-discs', 'parity-discs',
                                     'scratch-dir'])
    loglevel = logging.WARNING
    testing = False
    from_file_config = {}
    command_line_config = {}
    for o, v in opts:
        if o in ['-h', '--help']:
            usage()
            sys.exit(1)
        elif o in ['-v', '--verbose']:
            loglevel -= 10
        elif o in ['-n', '--dry-run']:
            command_line_config['actually_burn'] = False
            # warn user
            from time import sleep
            for i in range(25):
                for j in range(4):
                    print('not actually burning', end=' * ')
                print('\n')
            sleep(5)
        elif o in ['-t', '--test']:
            loglevel = logging.DEBUG
            testing = True
        elif o in ['-c', '--config']:
            with open(v) as f:
                from_file_config = yaml.load(f)
        elif o == '--disc-size-MiB':
            command_line_config['disc_size_MiB'] = int(v)
        elif o == '--data-discs':
            command_line_config['data_discs'] = int(v)
        elif o == '--parity-discs':
            command_line_config['parity_discs'] = int(v)
        elif o == '--scratch-dir':
            command_line_config['scratch_dir'] = v
        else:
            raise Exception('unknown switch {}'.format(o))
    _logging_setup(loglevel)
    log = logging.getLogger('__main__')
    log.debug('called with argv %r', sys.argv)
    if testing:
        # strip off switches: they are not for unittest.main
        sys.argv = [sys.argv[0]] + remaining
        sys.exit(unittest.main())
    final_config = {}
    final_config.update(config.defaults)
    final_config.update(from_file_config)
    final_config.update(command_line_config)
    if len(remaining) > 0:
        usage('unrecognized arguments {remaining!r}; '
              'need \'--\' before subcommand'.format(remaining=remaining))
    if len(subcommand_etc) < 1:
        usage('no subcommand given')
    try:
        sub = subcommands[subcommand_etc[0]]
    except KeyError:
        usage('unknown subcommand {}; expected one of {}'.format(
            subcommand_etc[0], list(subcommands.keys())))
        a = time.time()
        result = run(sub(s, *subcommand_etc[1:]))
        b = time.time()
        print('time elapsed: {:10.5f} sec; exit code: {}'.format(b-a, result))
        sys.exit(result)
    else:
        usage()
