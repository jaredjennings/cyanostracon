#!/usr/bin/python3
# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import curio.subprocess
import curio.io
import curio.sync
import sys
import os


class StdinStream(curio.io.FileStream):
    _lock = curio.sync.Lock()
    
    """Do Curio asynchronous input from stdin.

    This sets stdin to non-blocking, so if you do anything with it
    outside Curio, it won't work how you expect. Also it sets stdin to
    blocking when it's done, so if you use more than one of these in
    the same Curio environment, the wrong thing will happen. That's
    why it takes a lock.

    """
    def __init__(self):
        super().__init__(sys.stdin.buffer)

    async def __aenter__(self):
        await self._lock.acquire()
        
    async def __aexit__(self, *args):
        await self.flush()
        # don't close stdin! just return it to blocking I/O
        os.set_blocking(int(self._fileno), True)
        self._file = None
        self._fileno = -1
        await self._lock.release()

async def yesno(prompt, default=True):
    async with StdinStream() as stdin:
        valid_input = False
        while not valid_input:
            print(prompt, end='')
            sys.stdout.flush()
            # FIXME? assuming UTF-8 is bad, but assuming ASCII
            # would be worse - even though the valid inputs
            # are all ASCII.
            the_input = stdin.readline().strip().decode('UTF-8')
            if the_input == '':
                valid_input = True
                result = default
            elif (the_input.startswith('y') or
                  the_input.startswith('Y')):
                valid_input = True
                result = True
            elif (the_input.startswith('n') or
                  the_input.startswith('N')):
                valid_input = True
                result = False
            if not valid_input:
                print('Did not understand your input. Asking again.')
        return result

async def run_with_retry(parent_log, args):
    log = parent_log.getChild('run_with_retry')
    try_again = True
    async with StdinStream() as stdin:
        while try_again:
            log.info('running command {!r}'.format(args))
            try:
                curio.subprocess.run(args)
                try_again = False
            except subprocess.CalledProcessError as e:
                log.exception('an error was encountered '
                              'when running command {!r}'.format(args))
                valid_input = False
                while not valid_input:
                    print('Something went wrong when running command {!r}. '
                          'Try again? [Y/n] '.format(args), end='')
                    # FIXME? assuming UTF-8 is bad, but assuming ASCII
                    # would be worse - even though the valid inputs
                    # are all ASCII.
                    the_input = stdin.readline().strip().decode('UTF-8')
                    if the_input == '':
                        valid_input = True
                        try_again = True
                    elif (the_input.startswith('y') or
                          the_input.startswith('Y')):
                        valid_input = True
                        try_again = True
                    elif (the_input.startswith('n') or
                          the_input.startswith('N')):
                        valid_input = True
                        try_again = False
                        log.error('re-raising the error')
                        raise
                    if not valid_input:
                        print('Did not understand your input. Asking again.')


async def run_multiple_with_retry(parent_log, argss):
    log = parent_log.getChild('run_multiple_with_retry')
    try:
        async with TaskGroup() as g:
            for args in argss:
                t = await g.spawn(csp.check_output(args, stderr=csp.STDOUT))
            async for t in g:
                try:
                    t.result
                    log.info('task %r completed', t)
                except csp.CalledProcessError as e:
                    async with StdinStream() as stdin:
                        log.error('task %r, running %r, exited with nonzero '
                                  'code %d', t, e.cmd, e.returncode)
                        print(e.stdout.decode(sys.stdout.encoding),
                              file=sys.stderr)
                        valid_input = False
                        while not valid_input:
                            print('Something went wrong when running '
                                  'command {!r} (exitcode {}). Try again? '
                                  '[Y/n] '.format(e.cmd, e.returncode),
                                  end='')
                            sys.stdout.flush()
                            the_input = await stdin.readline()
                            the_input = the_input.strip().decode(
                                sys.stdin.encoding)
                            if the_input == '':
                                valid_input = True
                                try_again = True
                            elif (the_input.startswith('y')):
                                valid_input = True
                                try_again = True
                            elif (the_input.startswith('n')):
                                valid_input = True
                                try_again = False
                                log.error('giving up; re-raising error')
                                raise
                            if not valid_input:
                                print('Did not understand your input. '
                                      'Asking again.')
                        if try_again:
                            log.warning('spawning another! %r', e.cmd)
                            await g.spawn(csp.check_output(e.cmd,
                                                           stderr=csp.STDOUT))
    except TaskGroupError as e:
        for t in e:
            print('error: task', t, 'exception is', t.exception)
            print('           ', t, 'result is', t.result)


    log = parent_log.getChild('run_with_retry')
    try_again = True
    async with StdinStream() as stdin:
        while try_again:
            log.info('running command {!r}'.format(args))
            try:
                curio.subprocess.run(args)
                try_again = False
            except subprocess.CalledProcessError as e:
                log.exception('an error was encountered '
                              'when running command {!r}'.format(args))
                valid_input = False
                while not valid_input:
                    print('Something went wrong when running command {!r}. '
                          'Try again? [Y/n] '.format(args), end='')
                    # FIXME? assuming UTF-8 is bad, but assuming ASCII
                    # would be worse - even though the valid inputs
                    # are all ASCII.
                    the_input = stdin.readline().strip().decode('UTF-8')
                    if the_input == '':
                        valid_input = True
                        try_again = True
                    elif (the_input.startswith('y') or
                          the_input.startswith('Y')):
                        valid_input = True
                        try_again = True
                    elif (the_input.startswith('n') or
                          the_input.startswith('N')):
                        valid_input = True
                        try_again = False
                        log.error('re-raising the error')
                        raise
                    if not valid_input:
                        print('Did not understand your input. Asking again.')
