# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
from math import floor
from cyanostracon.has_settings import HasSettings


class Names(HasSettings):
    def _slice_name(self, basename, number_ob, extension):
        return '{{}}.{{:0{}d}}.{{}}'.format(self._.digits).format(
            basename, number_ob, extension)

    def disc_dir(self, disc_number):
        return '__disc{:04d}'.format(disc_number)

    def disc_dirs(self):
        for i in range(1, self._.discs_per_set+1):
            yield os.path.join(self._.scratch_dir, self.disc_dir(i))

    def data_dirs(self):
        for i in range(1, self._.data_discs+2):
            yield self.disc_dir(i)

    def parity_dirs(self):
        for i in range(self._.data_discs+1, self._.discs_per_set+2):
            yield self.disc_dir(i)

    def disc_title(self, basename, set_number_zb, disc_in_set_number_zb):
        # Max ISO 9660 vol id length is 32. Leave room for numbers and 2 dashes.
        # +1: These numbers are 0-based, but we want the ones in the title 1-based.
        # If you change the format here, change code above in last_set_directory!
        return "{}-{:04d}-{:03d}".format(basename[:(32-4-3-2)],
                                         set_number_zb + 1,
                                         disc_in_set_number_zb + 1)

    def set_number_zb_for_slice(self, slice_ob):
        return floor((slice_ob - 1) / self._.slices_per_set)

    def disc_title_for_slice_and_disc(self, basename, slice_ob,
                                      disc_in_set_number_zb):
        snzb = self.set_number_zb_for_slice(slice_ob)
        return self.disc_title(basename, snzb, disc_in_set_number_zb)

    def disc_titles_for_slice(self, basename, slice_ob):
        snzb = self.set_number_zb_for_slice(slice_ob)
        for disc_in_set_number_zb in range(self._.discs_per_set):
            yield self.disc_title(basename, snzb, disc_in_set_number_zb)

    def par_filename(self, basename, min_number, max_number):
        parformat = "{{}}.{0}-{0}.par".format(self._.number_format)
        return parformat.format(basename, min_number, max_number)

    @property
    def par_volume_format(self):
        return '{{}}.{0}-{0}.{{}}{{:02d}}'.format(self._.number_format)

    # FIXME needlessly duplicated code with
    # cyanostracon.par.par_volume_names
    def par_volume_names(self, basename, min_, max_, count):
        possible_volume_letters = 'pqrstuvxwyz'
        for i in range(count):
            # name.p00, name.p01, ..., name.p99, name.q00, ...
            letter = possible_volume_letters[i // 100]
            number = i % 100
            pfn = self.par_volume_format.format(basename, min_, max_,
                                                letter, number)
            yield pfn

    def disc_title_for_slice(self, basename, dar_slice_number):
        set_number = floor((dar_slice_number - 1) /
                self._.slices_per_set)
        disc_in_set_number = ((dar_slice_number - 1) % self._.data_discs)
        return self.disc_title(basename, set_number, disc_in_set_number)
