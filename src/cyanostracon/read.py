import re

async def curio_recorded_disc_adapter(readyqs, statusq,
                                      volume_names, *waiter_args):
    """Detect recorded discs having certain volume names.

    The insertdisc library is used to detect data discs having any of
    the given volume_names already inserted at the time multiburn
    begins, and thereafter. As soon as one of these discs is
    available, a read is started.

    """
    log = logging.getLogger('curio_recorded_disc_adapter')
    log.debug('started')
    mine, childs = Pipe()
    detector_task = await spawn(run_in_process(
        wait_for_disc_process, childs, *waiter_args), daemon=True)
    log.debug('spawned wait_for_disc_process which is %r', wait_for_disc_process)
    mine = Connection.from_Connection(mine)
    log.debug('sending query')
    for vn in volume_names:
        await mine.send(('named_data', vn))
    await mine.send(('go',))
    log.debug('awaiting data discs')
    while True:
        try:
            _blank, volume_name = await mine.recv()
            await readyqs[volume_name].put(devpath)
            await statusq.put((devpath, 'disc'))
        except CancelledError:
            await detector_task.cancel()
            break

def has_to_do_with(basename, number_check, filename):
    regexes = [
        r'{}.\(\d+\).dar',
        r'{}.\(\d+\)-\(\d+\).par',
        r'{}.\(\d+\)-\(\d+\).[p-z][0-9][0-9]',
        ]
    regexes = [x.format(re.escape(basename)) for x in regexes]
    for ret in regexes:
        m = re.match(ret, filename)
        if m:
            numbers = [int(x) for x in m.groups()]
            if number_check(int(numbers[0]),
                            int(numbers[1]) if len(numbers) > 1 else None):
                    return True
                    break
    else:
        return False
            
        
async def _copy_slices(basename, min_number, from_, to_):
    for fn in os.listdir(from_):
        if has_to_do_with(basename, lambda x: x >= min_number, fn):
            shutil.copyfile(pjoin(from_, fn), to_)
            # TODO: error handling. Unlike burning, copying things
            # from a disc can partially fail.

async def _provide_for_copy(basename, disc_title, disc_dir, min_number, readyq):
    devpath = await readyq.get()
    ## HANDWAVE: mount the disc
    dir = await mount_the_disc(devpath)
    await _copy_slices(basename, min_number, dir, disc_dir)
            
async def _fetch_slices(basename, first_slice_zb, last_slice_zb=None):
    log = logging.getLogger('_fetch_slices')
    log.debug('(%r, %r)', first_slice_zb, last_slice_zb)
    # we need entire parity sets, so if first_slice_zb is in the
    # middle of a set, we start at the beginning of the set
    first_slice_zb -= first_slice_zb % self.settings.data_discs
    set_number_zb = math.floor(first_slice_zb /
                               self.settings.slices_per_set)
    self.log.debug('for slice %r (zb) et seq we want set (zb) %d',
                   first_slice_zb, set_number_zb)
    for disc_number_zb in range(self._.discs_per_set):
        disc_title = self.disc_title(basename, set_number_zb, disc_number_zb)
        await spawn(_provide_for_copy(

async def _fetch_slices_ingenuity(self, basename,
                                  first_slice_zb, last_slice_zb=None):
    log = self.log.getChild('_fetch_slices_ingenuity')
    # MAYBE FIXME: we take the set of .par files on the first disc
    # of the set as authoritative; if any are missing I'm not sure
    # what would happen.
    disc_zb = 0
    disc_title = self.disc_title(basename, set_number_zb, disc_zb)
    disc_dir = self.written_disc_directory(disc_title)
    pars = sorted([x for x in os.listdir(disc_dir) if x.endswith('.par')])
    self.log.debug('pars: %r', pars)
    parity_set_ranges = [self._numbers_from_par_filename_zb(p) for p in pars]
    parity_sets_hereafter = [(a,b) for a,b in parity_set_ranges 
                             if a >= first_slice_zb]
    pars_hereafter = [self._par_filename(basename, a+1, b+1)
                      for a,b in parity_sets_hereafter]
    self.log.debug('pars_hereafter (after slice %d, %d in set %d): %r',
                   first_slice_zb,
                   first_slice_zb % self.settings.slices_per_set,
                   set_number_zb,
                   pars_hereafter)
    if last_slice_zb is None:
        max_last_slice_zb = parity_sets_hereafter[-1][-1]
        if (max_last_slice_zb - first_slice_zb) < (
                0.5 * self.settings.slices_per_set):
            # no use being smart
            self.log.debug('less than half a set left, fetching the rest')
            last_slice_zb = max_last_slice_zb
        else:
            try:
                with open(os.path.join(self.settings.scratch_dir,
                                       'last_fetched_fraction.txt')) as f:
                    last_fetched_set_zb = int(f.readline())
                    last_fetched_fraction = float(f.readline())
            except FileNotFoundError:
                last_fetched_set_zb = set_number_zb
                last_fetched_fraction = 0.03
            if last_fetched_set_zb != set_number_zb:
                # this file is out of date
                last_fetched_set_zb = set_number_zb
                last_fetched_fraction = 0.03
            to_fetch_fraction = last_fetched_fraction * 3
            self.log.debug('this time we are fetching %6.3f of a set',
                           to_fetch_fraction)
            max_to_fetch = int((to_fetch_fraction *
                                self.settings.slices_per_set) + 1)
            last_slice_zb = min(max_last_slice_zb,
                                first_slice_zb + max_to_fetch)
            with open(os.path.join(self.settings.scratch_dir,
                                   'last_fetched_fraction.txt'), 'wt') as f:
                print(last_fetched_set_zb, file=f)
                print(to_fetch_fraction, file=f)
    # we need entire parity sets, so if last_slice_zb is in the
    # middle of a set, we end at the end of the set
    for a, b in parity_sets_hereafter:
        if last_slice_zb >= a and last_slice_zb <= b:
            last_slice_zb = b
            break
    else:
        self.log.error('could not find which parity set slice %d is in',
                       last_slice_zb)
    self.log.debug('last_slice_zb is %d', last_slice_zb)
    for disc_zb in range(self.settings.total_set_count):
        disc_title = self.disc_title(basename, set_number_zb, disc_zb)
        disc_dir = self.written_disc_directory(disc_title)
        for f in os.listdir(disc_dir):
            if f.endswith('.dar'):
                n = self._number_from_slice_name_zb(f)
                if n >= first_slice_zb and n <= last_slice_zb:
                    self._copy(os.path.join(disc_dir, f),
                               os.path.join(self.settings.scratch_dir, f))
            elif parity_volume_re.match(f):
                a, b = self._numbers_from_par_filename_zb(f)
                if a >= first_slice_zb and b <= last_slice_zb:
                    self._copy(os.path.join(disc_dir, f),
                               os.path.join(self.settings.scratch_dir, f))
            elif f.endswith('.par'):
                if f in pars_hereafter:
                    self._copy(os.path.join(disc_dir, f),
                               os.path.join(self.settings.scratch_dir, f))
    for (a,b), parfilename in zip(parity_sets_hereafter, pars_hereafter):
        if a >= first_slice_zb and b <= last_slice_zb:
            self._run('parchive', 'r', parfilename)
