#!/usr/bin/python3
# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# these file operations are just imported and called...
from os import listdir, getcwd
from os.path import (
    exists as pexists, join as pjoin, dirname, basename)

import sys
import logging
import shutil
from itertools import cycle
from pkg_resources import resource_stream
from contextlib import closing

from curio import spawn, sleep
from curio.queue import Queue

import cyanostracon.dar
import cyanostracon.par
from cyanostracon.names import Names
from cyanostracon.has_settings import HasSettings
from cyanostracon.run_with_retry import run_with_retry
from cyanostracon.burn import multiburn, ask_user, curio_blank_disc_adapter

# ... whereas these file operations are awaited, in case some cool way
# to not block the process on them happens later
from cyanostracon.fs import (
    Scratch, mkdir, move, rmtree, copyfile, unlink, glob)

DAR_STATE_LAST_SLICE = 'last_slice'


class Cyanostracon(Names, Scratch, HasSettings):
    async def _make_disc_dirs(self):
        """Make disc directories.

        Assumes that disc directories do not exist.

        """
        for name in self.disc_dirs():
            await mkdir(pjoin(self._.scratch_dir, name))

    async def backup(self, dar_args):
        log = self.log.getChild('backup')
        disc_dirs = list(self.disc_dirs())
        await self._make_disc_dirs()
        par_tasks = []
        par_files = []
        unparitied_data_slice_numbers_ob = []
        unparitied_data_slices = []
        unburned_data_slices = []
        first_unburned_slice_ob = 1
        to_shard_q, from_shard_q = Queue(), Queue()
        # FIXME: how do we actually run cyanostracon, with the pex and all?
        dar = await spawn(cyanostracon.dar.dar_helper(self.log, self._, getcwd(), to_shard_q, from_shard_q, dar_args + ('-E', 'python /home/jaredj-local/cyanostracon/cyanostracon.py _shard %p %b %n %e %c')))
        #'echo SLICEY %p %b %n %e %c'))
        #
        # dar will run the shard, which will post into from_shard_q
        # that a new dar slice has been created.
        while True:
            news = await from_shard_q.get()
            try:
                log.debug('here is the news: %r', news)
                # "%p %b %n %e %c"
                path, basename, number_ob, extension, state = news[1:]
                number_ob = int(number_ob)
                slice_name = self._slice_name(basename, number_ob, extension)
                unparitied_data_slice_numbers_ob.append(number_ob)
                unparitied_data_slices.append(slice_name)
                unburned_data_slices.append(slice_name)
                if (
                        (len(unparitied_data_slices) == self._.data_discs)
                        or state == DAR_STATE_LAST_SLICE):
                    # make the parity slices for these data slices,
                    # asynchronously, so dar can get back to work at
                    # the same time
                    first_unp_ob = unparitied_data_slice_numbers_ob[0]
                    last_unp_ob = unparitied_data_slice_numbers_ob[-1]
                    pmtask = await spawn(self.par_and_move(
                        basename, first_unp_ob, last_unp_ob,
                        tuple(unparitied_data_slices)))
                    par_tasks.append(pmtask)
                    unparitied_data_slices.clear()
                slices_if_we_dont_burn = (
                    (len(unburned_data_slices) + 1)
                    * (self._.data_discs + self._.parity_discs)
                    // self._.data_discs)
                size_if_we_dont_burn_KiB = (
                    (slices_if_we_dont_burn * self._.slice_size_KiB)
                    + self._.reserve_space_KiB)
                if (
                        (size_if_we_dont_burn_KiB > self._.disc_size_KiB)
                        or state == DAR_STATE_LAST_SLICE):
                    # parity slices must be made before we can burn.
                    # also i think if one threw an error, this is
                    # where it may show up.
                    for t in par_tasks:
                        await t.join()
                    par_tasks.clear()
                    for dd in disc_dirs:
                        await self.place_code_and_documentation(dd, basename)
                    await self.place_data_slices(unburned_data_slices)
                    await self.burn(basename, number_ob, state, disc_dirs)
                    # now these "unburned" things are burned.
                    await self.empty_out(disc_dirs)
                    unburned_data_slices.clear()
            except Exception as e:
                await to_shard_q.put(('error',))
                raise
            else:
                await to_shard_q.put(('ok',))
            if state == DAR_STATE_LAST_SLICE:
                break
        # with dar having emitted the last_slice, joining the task
        # that ran it should be a quick affair.
        await dar.join()


    async def par_and_move(self, basename, first_slice_ob,
                           last_slice_ob, data_slices):
        with self.mkdtemp_rm('par') as dir:
            in_dir = lambda fn: pjoin(dir, fn)
            par_filename = self.par_filename(basename, first_slice_ob,
                                             last_slice_ob)
            await cyanostracon.par.par_a(self._.parity_discs,
                                         in_dir(par_filename), tuple(data_slices))
            assert pexists(in_dir(par_filename)), (
                'Expected par output file does not exist')
            for dd in self.disc_dirs():
                await copyfile(in_dir(par_filename),
                               pjoin(dd, par_filename))
            await unlink(in_dir(par_filename))
            parity_dirs = list(self.disc_dirs())[self._.data_discs:]
            for pv, dd in zip(
                    self.par_volume_names(basename,
                                          first_slice_ob, last_slice_ob,
                                          self._.parity_discs),
                    self.parity_dirs()):
                assert pexists(in_dir(pv)), (
                    'Expected par output volume does not exist')
                await move(in_dir(pv), dd)
            assert len(listdir(dir)) == 0, (
                'Bug: Not all par output files moved out of directory')
            self.log.info('par task at slice %r has finished', last_slice_ob)

    async def place_code_and_documentation(self, dir, basename):
        with closing(resource_stream('cyanostracon', 'README.tmpl')) as stream:
            template = stream.read().decode('UTF-8')
        with open(pjoin(dir, 'README.txt'), 'wt') as f:
            dar_fmt = '{}.{}.dar'.format(basename, self._.number_format)
            f.write(template.format(
                argv=sys.argv, darrc=self._.darrc, s=self._,
                td=self._.discs_per_set, dd=self._.data_discs,
                pd=self._.parity_discs,
                a_dar_slice_filename=dar_fmt.format(1),
                a_par_filename=self.par_filename(
                    basename, 1, self._.data_discs),
                a_par_volume_filename=list(self.par_volume_names(
                    basename, 1, self._.data_discs, 1))[0],
                ))
            
            
        await sleep(0)

    async def place_data_slices(self, data_slices):
        for slice, dir in zip(data_slices, cycle(self.data_dirs())):
            await move(slice, dir)

    async def burn(self, basename, number_ob, state, disc_dirs):
        log = self.log.getChild('burn')
        if self._.actually_burn:
            disc_titles = self.disc_titles_for_slice(basename, number_ob)
            await multiburn(tuple(zip(disc_titles, disc_dirs)),
                            self._.disc_media,
                            ask_user,
                            curio_blank_disc_adapter)
        else:
            set_number_zb = self.set_number_zb_for_slice(number_ob)
            prefix = "set{:04d}_".format(set_number_zb+1)
            for dd in disc_dirs:
                d = dirname(dd)
                b = basename(dd)
                ndd = pjoin(d, prefix+b)
                await rename(dd, ndd)

    async def empty_out(self, disc_dirs):
        # ASSUMPTION: disc dirs do not contain subdirs
        for disc_number_zb, dir in enumerate(disc_dirs):
            all = await glob(pjoin(dir, '*'))
            for x in all:
                await unlink(x)




    def _extract(self, dir, basename, number, extension, happening):
        number = int(number)
        if number == 0:
            # dar wants the last slice but doesn't know its number
            self._fetch_some_slices(basename,
                                    *self._last_parity_set_slices_zb(basename))
        else:
            slice_name = self._slice_name(basename, number, extension)
            if os.path.exists(slice_name):
                # the first time this gets called with a real number,
                # happening is still 'init' so the hawkeyed will see
                # one of these messages before we go back to set 1
                self.log.debug('the file for slice (ob) %s already exists',
                               number)
                return
            else:
                self._fetch_some_slices(basename, number)

    _list = _extract
