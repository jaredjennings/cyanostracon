# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
from cyanostracon.run_with_retry import run_with_retry


def par_volume_names(par_filename):
    """Yield all possible PAR volume filenames for the given par_filename.

    If you want less than this, use itertools.islice.

    """
    if par_filename.endswith('.par'):
        stem = par_filename[:-len('.par')]
    else:
        raise ValueError('par filename does not end with .par')
    for letter in 'pqrstuvwxyz':
        for number in range(100):
            yield stem + '.{}{:02d}'.format(letter, number)


async def par_a(volume_count, par_filename, input_files):
    log = logging.getLogger('par_a')
    dash_n = '-n{}'.format(volume_count)
    args = (('par', 'a',  dash_n, '--', par_filename)
            + tuple(input_files))
    log.debug('run_with_retry with args %r', args)
    await run_with_retry(log, args)
