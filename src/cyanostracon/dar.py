# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging
from cyanostracon.subcommand import subcommand
from random import getrandbits
from curio.subprocess import Popen
from curio.channel import Channel, AuthenticationError
from curio.task import sleep

CYANOSTRACON_IPC_AUTHKEY_ENVVAR = 'CYANOSTRACON_IPC_AUTHKEY'
TCP_PORT = 17241 # arbitrarily chosen as ord('C') << 8 + ord('Y')

async def dar_helper(parent_log, config, cwd,
                     to_shard_q, from_shard_q, dar_args):
    """Run dar; help caller deal with requests for slices.

    dar, as we run it, writes slice files when backing up and wants to
    read them when restoring. It can run a program when it has
    finished a slice, or wants one, and we make it do this. The _shard
    function is what should be run in this case.

    """
    log = parent_log.getChild('dar')
    authkey = '{:x}'.format(getrandbits(256))
    authkey_bytes = authkey.encode('UTF-8')
    l = Channel(('127.0.0.1', TCP_PORT))
    await l.bind()
    os.environ[CYANOSTRACON_IPC_AUTHKEY_ENVVAR] = authkey
    dar_process = Popen(('dar',) + dar_args, cwd=cwd)
    state = None
    while state != DAR_STATE_LAST_SLICE:
        try:
            log.debug('accepting')
            ch = await l.accept(authkey=authkey_bytes)
        except AuthenticationError as e:
            log.error('rejecting connection: %s', e)
        else:
            # i expect only one connection at a time, so no
            # spawning to deal with the connection
            log.debug('channel opened: %r', ch)
            the_stuff = await ch.recv()
            await from_shard.put(the_stuff)
            # this depends on %c being last in the args
            state = the_stuff[-1]
            # this could block while we burn discs
            the_money = await to_shard_q.get()
            log.debug('sending the money %r', the_money)
            await ch.send(the_money)
            log.debug('sent, closing')
            await ch.close()
    await l.close()
    await from_shard_q.put(('end',))
    await dar_process.wait()

@subcommand
async def _shard(config, *args):
    """Let cyanostracon know that dar said something about a slice.

    Connect to the main cyanostracon process, by means given in
    the process environment, and get the message through. Wait for
    clearance to continue.

    """
    log = logging.getLogger('shard[{}]'.format(os.getpid()))
    authkey = os.getenv(CYANOSTRACON_IPC_AUTHKEY_ENVVAR, '').encode('UTF-8')
    log.debug('starting Client')
    c = await Channel(('127.0.0.1', TCP_PORT))
    await c.connect(authkey=authkey)
    the_stuff = ('slice',) + tuple(args)
    log.debug('sending %r', the_stuff)
    await c.send(the_stuff)
    log.debug('awaiting result')
    the_money = await c.recv() # this might take a while
    log.debug('received result %r', the_money)
    await c.close()
    if the_money[0] == 'ok':
        log.debug('exiting ok')
        return 0
    else:
        log.debug('exiting with error')
        return 1
