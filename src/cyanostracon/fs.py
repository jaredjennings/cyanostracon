# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil
import tempfile
import logging
import glob as _glob
from contextlib import contextmanager
from cyanostracon.has_settings import HasSettings

async def move(*args, **kwargs):
    log = logging.getLogger('mkdir')
    log.debug('*%r, **%r', args, kwargs)
    return shutil.move(*args, **kwargs)

async def rmtree(*args, **kwargs):
    log = logging.getLogger('mkdir')
    log.debug('*%r, **%r', args, kwargs)
    return shutil.rmtree(*args, **kwargs)

async def mkdir(*args, **kwargs):
    log = logging.getLogger('mkdir')
    log.debug('*%r, **%r', args, kwargs)
    return os.mkdir(*args, **kwargs)

async def copyfile(*args, **kwargs):
    log = logging.getLogger('mkdir')
    log.debug('*%r, **%r', args, kwargs)
    return shutil.copyfile(*args, **kwargs)

async def unlink(*args, **kwargs):
    log = logging.getLogger('mkdir')
    log.debug('*%r, **%r', args, kwargs)
    return os.unlink(*args, **kwargs)

async def rename(*args, **kwargs):
    log = logging.getLogger('mkdir')
    log.debug('*%r, **%r', args, kwargs)
    return os.rename(*args, **kwargs)

async def glob(*args, **kwargs):
    return _glob.glob(*args, **kwargs)

class Scratch(HasSettings):
    @contextmanager
    def mkdtemp_rm(self, prefix):
        dir = tempfile.mkdtemp(prefix=prefix, dir=self._.scratch_dir)
        yield dir
        shutil.rmtree(dir)
