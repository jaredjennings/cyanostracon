#!/usr/bin/python3
# -*- encoding: utf-8 -*-
# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# structure of setup.py from
# <https://blog.ionelmc.ro/2014/05/25/python-packaging/>

from setuptools import setup, find_packages
from os.path import basename, dirname, join, splitext
from glob import glob
import io

def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()

setup(
    name='cyanostracon',
    version='0.1.0',
    license='GPLv3',
    description='dar-based redundant backup to optical discs',
    long_description=read('README.rst'),
    author='Jared Jennings',
    author_email='cyanostracon@jjennings.fastmail.fm',
    url='https://gitlab.com/jaredjennings/cyanostracon',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    package_data={'cyanostracon': ['README.tmpl']},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
# https://stackoverflow.com/a/13783919
#    include_package_data=True,
    zip_safe=True,
    classifiers=[],
    keywords=[],
    extras_require={},
    entry_points={
        'console_scripts': [
            'cyanostracon = cyanostracon.main:main',
        ]})
