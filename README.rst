cyanostracon
============

This, like `darbrrb`_ before it, is dar-based Blu-Ray redundant
backup. It is designed to back up a few hundred gigabytes of my data
at home onto dozens of optical discs in a way that it can be restored
ten years later. But `cyanostracon` is made to burn or read from
multiple discs at once.

.. _`darbrrb`: https://github.com/jaredjennings/darbrrb

Threats to this goal:

 * Software changes.
 * Forgetting how I ran the backup.
 * Media failure (on part or all of a disc).
 * Data size scaling up.

Design responses:

 * cyanostracon is a self-contained Python 3 program. It runs `dar`_,
   `par`_ 1, and `growisofs`, which have been around a long time. It
   uses the command line, which has been around a long time.
 * cyanostracon places a copy of itself, and documentation including
   how it was run, on every backup disc.
 * cyanostracon makes dar write slice files much smaller than a single
   disc, and spreads the slices over sets of discs. It's sort of like
   RAID, but with optical discs, and you can configure the size of a
   set and the number of spares.
 * cyanostracon supports discs of any size you can burn. If you have
   multiple optical disc drives, cyanostracon can do its best to keep
   them all busy at the same time.

.. _`par`: http://en.wikipedia.org/wiki/Parchive
.. _`dar`: http://dar.linux.free.fr


Software requirements
---------------------

To run cyanostracon, you need (later versions are fine):

 * Python 3.5
 * dar 2.5.4
 * parchive 1.1
 * growisofs 7.1
 * genisoimage 1.1.11

dar 2.5.4 contained a `fix`_ for a bug involving the combination of
slices (which cyanostracon uses) and encryption.

.. _`fix`: https://sourceforge.net/p/dar/bugs/183/
