# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from glob import glob
import random
from cyanostracon import Cyanostracon
from cyanostracon.config import usable_config
import cyanostracon.par
from tempfile import TemporaryDirectory, mkdtemp
import shutil
from curio import run, sleep
from curio.subprocess import CompletedProcess
import sys
import time
import logging
from unittest.mock import patch
from tools import Fixture, cwd, temp_dir_keep_if_fail
from pytest import mark, fail
from contextlib import contextmanager, ExitStack


def once_wfdp(multiburn_pipe):
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    log = logging.getLogger('tattle_wfdp')
    while True:
        thing = multiburn_pipe.recv()
        log.debug('received %r', thing)
        if thing[0] == 'go':
            break
    # we need seven blank discs to appear in order to finish the
    # test. each burn takes 1.5 seconds (below).
    for x in range(7):
        to_send = ('blank', '/dev/snorkel0'.format(x))
        log.debug('sending %r', to_send)
        multiburn_pipe.send(to_send)
        log.debug('sleeping 2')
        time.sleep(0.8)

async def stub_working_burn_task(resource, spec):
    await sleep(0.5)
    exitcode = 0
    result = CompletedProcess('stub_burn_task {} {}'.format(resource, spec),
                              exitcode,
                              'output {} {}'.format(resource, spec),
                              'stderr {} {}'.format(resource, spec))
    return result

async def nop(*args, **kwargs):
    await sleep(0)

def test_run_a_backup_no_real_slices():
    """Run a backup, without actually running dar.

    This is not exhaustive but the minimum viable backup test.
    """
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    with temp_dir_keep_if_fail() as temp_dir:
        settings = usable_config({
            'scratch_dir': temp_dir,
            'basename': 'fnozzle',
        })
        fix = Fixture(settings)
        async def mock_check_output(*args, **kwargs):
            log = logging.getLogger('mock_check_output')
            log.info('mock_check_output, args %r, kwargs %r', args, kwargs)
        with ExitStack() as es:
            es.enter_context(
                patch('cyanostracon.burn.wait_for_disc_process',
                      new=once_wfdp))
            es.enter_context(
                patch('cyanostracon.par.par_a',
                      new=fix.make_files_mock_par_a()))
            es.enter_context(
                patch('cyanostracon.dar.dar_helper',
                      new=fix.make_queue_mock_dar(15)))
            es.enter_context(
                patch('cyanostracon.Cyanostracon.empty_out', new=nop))
            es.enter_context(
                patch('cyanostracon.burn.burn_task',
                      new=stub_working_burn_task))
            c = Cyanostracon(settings=settings)
            # queue_mock_dar does not construct dar files
            c.place_data_slices = nop
            run(c.backup, ('-v', '-c', 'test', '-R',
                           '/home/jaredj-local/some-files', '-s', '1k'))
        assert len(glob(os.path.join(temp_dir, '__disc*', 'README.txt'))) > 0
        with cwd(temp_dir):
            print(open('__disc0001/README.txt').read())


def test_par_and_move():
    for trial in range(12):
        data_discs = random.choice(range(3, 40))
        parity_discs = random.choice(range(4, 20))
        with temp_dir_keep_if_fail() as temp_dir:
            settings = usable_config({
                'data_discs': data_discs,
                'parity_discs': parity_discs,
                'scratch_dir': temp_dir,
                'basename': 'fnozzle',
            })
            fix = Fixture(settings)
            with patch('cyanostracon.par.par_a', new=fix.make_files_mock_par_a()):
                c = Cyanostracon(settings=settings)
                before = os.listdir(temp_dir)
                run(c._make_disc_dirs)
                after = os.listdir(temp_dir)
                assert len(after) > len(before)
                # normally the backup method handles chdiring
                with cwd(temp_dir):
                    # default digits is 6
                    run(c.par_and_move, 'foo', 1, data_discs, tuple(
                        ['foo.{:06d}.dar'.format(x+1) for x in range(data_discs)]))
                    all_disc_dirs = sorted(glob('__disc*'))
                    assert len(all_disc_dirs) == data_discs + parity_discs
                    data_disc_dirs = all_disc_dirs[:data_discs]
                    parity_disc_dirs = all_disc_dirs[-parity_discs:]
                    for pdd in parity_disc_dirs:
                        pvol_files = glob(os.path.join(
                            pdd, 'foo.000001-{:06d}.p[0-9]*'.format(data_discs)))
                        assert len(pvol_files) == 1
                    for dd in all_disc_dirs:
                        assert len(glob(os.path.join(dd, '*.par'))) == 1


if __name__ == '__main__':
    test_cyanostracon()
