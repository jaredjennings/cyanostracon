from multiprocessing import Pipe
from cyanostracon.burn import *
from curio import run, sleep, spawn
from curio.queue import Queue
from curio.subprocess import CompletedProcess
import logging
import sys
import time
from unittest.mock import patch, MagicMock
from contextlib import ExitStack

def tattle_wfdp(multiburn_pipe, return_to_test_pipe):
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    log = logging.getLogger('tattle_wfdp')
    while True:
        thing = multiburn_pipe.recv()
        log.debug('received %r', thing)
        return_to_test_pipe.send(thing)
        if thing[0] == 'go':
            break
    log.debug('sleeping 1')
    time.sleep(1)
    to_send = ('blank', '/dev/snorkel')
    log.debug('sending %r', to_send)
    multiburn_pipe.send(to_send)
    log.debug('sleeping 1')
    time.sleep(1)

def sockpuppet_wfdp(multiburn_pipe, return_to_test_pipe, control_from_test_pipe):
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    log = logging.getLogger('sockpuppet_wfdp')
    while True:
        thing = multiburn_pipe.recv()
        log.debug('received %r', thing)
        return_to_test_pipe.send(thing)
        if thing[0] == 'go':
            break
    while True:
        thing = control_from_test_pipe.recv()
        log.debug('received %r', thing)
        multiburn_pipe.send(thing)
        log.debug('sent it to multiburn')

# https://stackoverflow.com/a/32498408
class AsyncMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)

async def stub_working_burn_task(resource, spec):
    await sleep(0.5)
    exitcode = 0
    result = CompletedProcess('stub_burn_task {} {}'.format(resource, spec),
                              exitcode,
                              'output {} {}'.format(resource, spec),
                              'stderr {} {}'.format(resource, spec))
    return result

def test_insertdisc_adapter():
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    with patch('cyanostracon.burn.wait_for_disc_process',
               new=tattle_wfdp):
        tparent, tchild = Pipe()
        readyq = Queue()
        statusq = Queue()
        assert(readyq.empty())
        assert(statusq.empty())
        run(curio_blank_disc_adapter(readyq, statusq, 'optical_bd_r', tchild), timeout=2)
        assert(not readyq.empty())
        assert(not statusq.empty())
        r = run(readyq.get())
        assert r == '/dev/snorkel'
        s = run(statusq.get())
        assert s == ('/dev/snorkel', 'ready')

async def nop(*args):
    await sleep(0.1)

def test_multiburn_success():
    t1 = time.time()
    with ExitStack() as es:
        es.enter_context(patch(
            'cyanostracon.burn.wait_for_disc_process', new=tattle_wfdp))
        es.enter_context(patch(
            'cyanostracon.burn.burn_task', new=stub_working_burn_task))
        es.enter_context(patch(
            'cyanostracon.burn.eject', new=nop))
        tparent, tchild = Pipe()
        td = (('title1', '/dir/1'),)
        result = run(multiburn(td, 'optical_bd_r',
                               just_try_again, curio_blank_disc_adapter,
                               tchild))
        print(result)
    t2 = time.time()
    duration = t2-t1
    # it should take less than 2 seconds in fact.
    assert (duration < 3.0), "took too long"

def test_multiburn_multidisc_no_failures():
    t1 = time.time()
    with ExitStack() as es:
        es.enter_context(patch(
            'cyanostracon.burn.wait_for_disc_process', new=sockpuppet_wfdp))
        es.enter_context(patch(
            'cyanostracon.burn.burn_task', new=stub_working_burn_task))
        es.enter_context(patch(
            'cyanostracon.burn.eject', new=nop))
        tattle_parent, tattle_child = Pipe()
        control_parent, control_child = Pipe()
        async def blank_program(p):
            await sleep(1)
            p.send(('blank', '/dev/snorkel1'))
            await sleep(1)
            p.send(('blank', '/dev/snorkel2'))
            await sleep(1)
        td = (('title1', '/dir/1'), ('title2', '/dir/2'))
        async def the_test():
            mbtask = await spawn(multiburn(td, 'optical_bd_r',
                                   just_try_again, curio_blank_disc_adapter,
                                   tattle_child, control_child))
            blank_emitter_task = await spawn(blank_program(control_parent),
                                             daemon=True)
            result = await mbtask.join()
            print(result)
            blank_emitter_task.cancel()
        run(the_test())
    t2 = time.time()
    duration = t2-t1
    # it should take less than 2 seconds in fact.
    assert (duration < 3.0), "took too long"
