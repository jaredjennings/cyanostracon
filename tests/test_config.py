# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from cyanostracon.config import (
    UnitConverter, ItemsToAttributes, SomeCalculated,
    usable_config)

def test_UnitConverter():
    u = UnitConverter({'foo_KiB': 1024})
    assert u['foo_MiB'] == 1
    u = UnitConverter({'foo_MiB': 5})
    assert u['foo_MiB'] == 5
    assert u['foo_KiB'] == 5120
    assert u['foo_GiB'] == 0.0048828125

def test_UnitConverter_contains():
    u = UnitConverter({'bar_KiB': 8192})
    assert 'bar_KiB' in u
    assert 'bar_MiB' in u
    assert 'foo_KiB' not in u

def test_UnitConverter_length():
    u = UnitConverter({'bar_KiB': 8192})
    assert len(u) == 3
    assert set(u.keys()) == set(['bar_KiB', 'bar_MiB', 'bar_GiB'])

def test_ItemsToAttributes():
    d = {'three': 5}
    ad = ItemsToAttributes(d)
    assert ad.three == 5

def test_ITAUC():
    d = {'foo_KiB': 10240}
    u = UnitConverter(d)
    ita = ItemsToAttributes(u)
    assert ita.foo_MiB == 10

def test_calculated():
    calculate = {
        'foo_plus_two': lambda x: x['foo'] + 2,
    }
    sc = SomeCalculated({'foo': 2}, calculate)
    assert sc['foo_plus_two'] == 4

def test_usable_1():
    u = usable_config({})
    assert u.slice_size_KiB

def test_real_settings_conversion():
    """A thing that failed in test_run_a_backup_no_real_slices once.

    Aha, the SomeCalculated was wrapping the UnitConverter. The
    SomeCalculated provides slice_size_KiB.

    """
    settings = usable_config({
        'scratch_dir': 'fnord',
        'basename': 'fnozzle',
    })
    print(settings._here._here)
    print(settings._here['slice_size_KiB'])
    print(settings._here['slice_size_MiB'])
    print(settings.slice_size_KiB)
    print(settings.slice_size_MiB)
