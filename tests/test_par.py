# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from curio import run
from os import getcwd
from os.path import exists, dirname
from cyanostracon.config import usable_config
from cyanostracon.par import par_volume_names
from tools import Fixture, mkdtemp_rm, cwd
from pytest import raises

def test_par_volume_names():
    with raises(ValueError, message='non-par-looking filename '
                'passed in, but no exception raised'):
        allofem = list(par_volume_names('foo'))
    allofem = list(par_volume_names('foo.par'))
    assert len(allofem) == 1100

def test_mock_par():
    settings = usable_config({
        'scratch_dir': '.',
        'basename': 'fnozzle',
    })
    orig_cwd = getcwd()
    fix = Fixture(settings)
    par_a = fix.make_files_mock_par_a()
    make_sure_dir = None
    with mkdtemp_rm('test_par') as dir:
        # dir should be a subdirectory of our original cwd
        assert dirname(dir) == orig_cwd
        with cwd(dir):
            make_sure_dir = dir
            run(par_a(8, 'foo.par', ('a', 'b', 'c')))
            assert exists('foo.par')
            assert exists('foo.p00')
            assert exists('foo.p01')
            assert exists('foo.p02')
            assert exists('foo.p03')
            assert exists('foo.p04')
            assert exists('foo.p05')
            assert exists('foo.p06')
            assert exists('foo.p07')
    assert not exists(make_sure_dir)
