# cyanostracon: dar-based redundant backup to optical discs.
# Copyright 2016, Jared Jennings <jjenning@fastmail.fm>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from contextlib import contextmanager
from itertools import islice
from curio import sleep
import logging
import tempfile
import os
import shutil

# yes, this imports code from cyanostracon. this code is tested elsewhere.
from cyanostracon.par import par_volume_names

class Fixture:
    def __init__(self, config):
        self.log = logging.getLogger('TestFixture')
        self.config = config

    def make_queue_mock_dar(self, nslices, errors_at=()):
        """Send and receive messages on queues imitating a real dar and shard."""
        async def the_mock_dar(parent_log, config, cwd,
                               to_shard_q, from_shard_q, dar_args):
            log = parent_log.getChild('queue mock dar with {} slices'.format(
                nslices))
            for slice_ob in range(1, nslices + 1):
                log.debug('slice_ob %d', slice_ob)
                await from_shard_q.put(('slice', cwd, config.basename,
                                        slice_ob, 'dar',
                                        ('operation' if slice_ob < nslices
                                         else 'last_slice')))
                await sleep(1.0 / nslices) # some but not much
                the_money = await to_shard_q.get()
        return the_mock_dar

    def make_files_mock_par_a(self):
        """Create all the files ``par a`` would."""
        async def the_mock_par(volume_count, par_filename, input_files):
            stem = par_filename[:par_filename.rindex('.')]
            def touch(filename):
                with open(filename, 'wt') as f:
                    f.write('.\n')
            touch(par_filename)
            for volume_name in islice(par_volume_names(par_filename),
                                      volume_count):
                touch(volume_name)
        return the_mock_par

@contextmanager
def cwd(dir):
    """Do something inside a given directory."""
    log = logging.getLogger('cwd')
    oldcwd = os.getcwd()
    log.debug('cwd was %r', oldcwd)
    log.debug('cd %r', dir)
    os.chdir(dir)
    yield oldcwd
    log.debug('cd %r', oldcwd)
    os.chdir(oldcwd)


@contextmanager
def mkdtemp_rm(prefix):
    """Make a tmp subdirectory of cwd; cd into it; cd out and rm when done."""
    the_dir = tempfile.mkdtemp(prefix=prefix, dir=os.getcwd())
    try:
        yield the_dir
    finally:
        shutil.rmtree(the_dir)

@contextmanager
def temp_dir_keep_if_fail(*args, **kwargs):
    d = tempfile.mkdtemp(*args, **kwargs)
    try:
        yield d
        shutil.rmtree(d)
    except:
        raise
