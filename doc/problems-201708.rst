What if it fails?
-----------------

Step 0. darbrrb. Discs burned serially; if one fails, we ask if the
user wants to retry. "Insert disc and press Enter." But we spend too
much time burning.

Step 1. insertdisc written. We can detect newly inserted blank
discs. cyanostracon begun to do parallel, event-based parity
calculations and burns. But the serial burn logic is held over from
darbrrb.

Step 2. multiburn. We will spawn many growisofs processes to burn
discs. But if a burn were to fail, we would need to ask for a retry,
and the question would be lost in the sea of output from the other
growisofs processes.

Step 3. run_multiple_with_retry. We will grab stdout and stderr of the
burn processes, and disgorge these for one process if it fails, asking
for a retry. The use of stdin involves a lock, and we are
interrogating for failure sequentially, so anyhow we will not barf
multiple stderrs out at once. But if a burn fails, the next new burn
will grab up the next available blank disc, not the retry.

Step 4. problemq. Each burn process will put something to a queue it
shares with the multiburn task, and if there are any problems, we will
use the next available blank disc for a retry, not a new burn. But how
will the retrying burn process know to use the (possibly different)
drive that the blank disc is in?

Step 5. (Solutions beginning to be jumbled with the next problem.)
Munge run_multiple_with_retry into multiburn, so it can issue a
different growisofs command with a different device for the retry. But
how will the user know which disc(s) failed to burn, and which burned
successfully and should be kept? We could tell the user the name of
the device, but we can't assume the user can be bothered to remember
which physical device goes with which name.

At this point it is assumed that we have a bunch of unlabelled discs,
all belonging to a single set (if the work done in between burns is
quick enough, we may not even know one set from another). They are
unlabelled because the user cannot know in which order several
pre-inserted discs will be burned, and thus which will be (WLOG) disc
one of the set. The problem is how to distinguish successfully burned
discs from those unsuccessfully burned.

Failures could be distinguished by time of ejection: when a burn
fails, that drive is not used until all other discs in the set have
been burned, or no drives remain that aren't in an error state. Then
failure is signalled (so the user can set aside the successfully
burned discs), all burn failures are ejected, and we try again if the
user wants to. But repeated failures could leave no drives to burn
with, and cyanostracon doesn't know how many drives there are anyway.

We could hope that failed burns will always be visually
distinguishable from successful burns, so that at the end of the set
the user can determine which discs to throw away. But that may not be
true.

We could require some external indicator of status, such as a row of
lights, one by each drive, that we could control. This decimates the
potential userbase, which was probably small enough already.

Roughly equivalent, but much cheaper and simpler, would be to depend
on the user to label drives with their device names, and show errors
with device names.

We could fail the whole backup whenever a burn fails; but that is too
brittle. It wastes the user's time and media.

Another try at time of ejection: when a burn fails, no more burns are
begun until the failure is acknowledged, and, presumably, the user can
get rid of the failed medium. The problem with this is that multiple
burns should usually be happening at once, and if the user doesn't get
to the failed burn soon enough, several successful burns have been
ejected, and now which is the failed one? Perhaps all the discs are
kept in except the failed one - but a burn could succeed before the
failure. So keep all discs in until they have all successfully
burned. If one fails, eject just that one. If all succeed, eject them
all at once. This introduces a barrier on multiple discs burning at
once - they can't just start and finish whenever without dependencies
on the others being burned - but the individual burns will likely take
close to the same amount of time anyway. But if all the discs fail to
burn, they will be ejected one by one, until the whole set is ejected,
the same result in terms of ejections as a successful ... shall we
call it a heat? Except that the user will have been notified.

Oh, all this work to enable the use of stdin, and that may not be the
best means of interaction. Jabber, an LCD, or a mobile app, perhaps?

The end of this one (Oct 2017)
------------------------------

The way this shook out was, we just depend on the user to know which
device name is what drive. Nothing about the notification yet.
