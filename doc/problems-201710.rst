Reading that which was burned
-----------------------------

We want to read discs the user has burned, to extract some part of the
dar file. It is simplest to read entire sets, and recover whatever is
recoverable: if this fails, we have failed. But that is slow. First of
all, dar always wants the last slice first. Then, if the user hasn't
requested that the entire archive be extracted, dar wants slices
starting with a given slice number. We never know how many. Also,
while failure must be dealt with, it is possible to succeed quicker
than this trivial case, if everything goes right and proper attention
is paid.

To strike a balance between reading the minimum number of needed
slices in case a small part of the archive is being extracted, and
requiring the minimum number of disc switches necessary, darbrrb
started by copying a few slices from each disc, and the more slices in
a row that were requested, the more additional slices it would fetch
next time.

But darbrrb was more interactive than we are trying to be, and more
serial. If it didn't matter which disc from a set you put in, that
would be much nicer. If we could succeed as soon as we have enough
data, that would be cool. But there's still the problem of what to do
if there really isn't enough data to recover. If you are in front of
the console you can Ctrl-C, but if you are using another input method
there should probably be some way of saying back, "this is not going
to work, stop trying."

Iteratively expressed
---------------------

Step 0. Every time dar requires a slice, read the whole set containing
the slice into the scratch directories. Then hand dar its slice. Don't
run par. - This is slow, silly, and prone to failure.

Step 1. Pay attention to whether the slice requested is in the same
set we have already read. After reading a set, run par to recover
whatever needs to be recovered and can be recovered. - Slow, not quite
as silly, not quite as prone to failure. But dar always wants the last
slice first, then it wants to start reading the set containing the
files requested. Reading the entire last set for one slice is a lose.

Step 2. Keep a number of par volume sets we will fetch at once, and
the last slice requested. Whenever the next slice requested is not the
next one after the previous slice requested, reset the prefetch
to 1. After each disc's files are read, try to reconstruct the
requested dar slice (because this is cheap to do). If we just copied
the slice and its checksum is ok, we have reconstructed the
slice. Otherwise, keep asking for more discs. Here we may take some
inspiration from code in kernels that reads blocks of files from disks
into RAM? It is perhaps important to remember that keeping one of
several discs inserted and copying more from it to reduce disc
switching is no more nor less important than keeping a single disc
inserted to read more off of it: meaning, I don't think multiple
drives magically change the tradeoff between avoiding reading ahead
tok much and avoiding too many disc switches. As for reading all that
can be read and not having enough to recover, at this stage perhaps we
can do with requiring the user to kill cyanostracon; it certainly
can't do any more useful in that case, other than convey condolences
on lost data.
