run cyanostracon. the main process is called the Senate. (not quite
good metaphor.) it listens for Calls, among which can be requests to
run a process. The environment for processes run contains a variable
enabling communication back with the Senate. The main process starts a
single Task which makes a Call to run dar with the darrc and
switches. Control will not return back to this Task until dar has
finished, with some exit code.

alternately: run cyanostracon. The main task runs dar, and iterates
over the slices it creates. The dar-runner cleverly transmutes slice
events from dar-run Shards into values yielded from an
iterator. Except each value needs to be acknowledged, so that the
Shard can block until (e.g.) the burn is complete... this makes
iteration bi-directional, and thus weird enough to not be a protocol
to use. But the main task retains control somehow, rather than
"listening." this is curio, we shouldn't have to write using an
event-based metaphor.

dar runs cyanostracon with funky args, resulting in a ... shall we
say, Shard. The Shard uses its environment to communicate back with
the Senate, and make Calls. It makes Calls to find out settings, and
to request par runs and burns. Now wait: does the Shard know when a
burn should happen, and request it of the Senate, which request does
not return until the burn is complete; or does the Shard just tell the
Senate that such-and-such dar slice is done, and the Senate decides to
spawn a par and/or burn before yielding control back to the Shard?

It seems attractive to make the code of the Shard as small as
possible: it is really just a way to propagate the event that dar has
finished writing a slice back to cyanostracon, and to compel dar to
wait as necessary. 

Dependencies: Each burn depends on all pars having finished that are
necessary for the disc set; each par depends on its set of dar
slices. Cleaning out for the next disc set depends on the burn
completing. But that's only the backup phase.

Perhaps it is easier to start all the tasks for a disc set ahead of
time and have them start by waiting for their dependencies - rather
than spawning the tasks at the time of need. Ah, but we don't know how
many dar slices there will be.
