Zooming out from the problems in problems-201710.rst, insertdisc needs
to grow some new capabilities before cyanostracon can read discs.

insertdisc is capable of detecting a blank disc of the correct type in
any drive attached to the system, using udisks2, at this point. And it
can detect a blank disc with a certain label, or even iirc with a
label matching a certain regex. But it can't mount or eject discs, and
the protocol for talking about labelled discs is sort of strained:
having to multiplex the event messages because they came from multiple
coroutines in the client is stilted as I recall.

Also insertdisc requires python-dbus to be installed, which is
unpippable. dbussy now exists, which ought to be much nicer. There was
some flopping about regarding how to mock D-Bus messages using dbussy,
since it uses ctypes to directly interface with the D-Bus libraries;
but that cleared up, right? Mock at the boundary with dbussy. But it's
a rewrite.

Also insertdisc is supposed to be multiparadigm: udisks2 is only one
way of detecting discs, that only works for some people. A fallback
would be nice to have, and it wouldn't involve D-Bus at all, which,
with or without dbussy, adds weight to changes.

There should be a manual backend. But it will have to handle the idea
that multiple requests can be in flight at once, and you may not want
to answer the one you just got asked about. So the UI needs to be as
multithreaded as the tasks are.
